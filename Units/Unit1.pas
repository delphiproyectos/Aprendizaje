unit Unit1;

interface
  var
    variable: Integer;

  const
    constante = 1;

  //Se pueden agregar tipos, variables, etc....

  function Suma(x, y: Integer) : Integer;

implementation

  uses
    Unit2;

  function Suma(x, y: Integer) : Integer;
  begin
    Result := x + y;

    Resta(Result, 0);

    Unit2.Resta(Result, 0); // Mas detallado
  end;

end.
