unit Unit2;

interface
   function Resta(x, y: Integer) : Integer;
implementation
   function Resta(x, y: Integer) : Integer;
   begin
     Result := x - y;
   end;
end.
