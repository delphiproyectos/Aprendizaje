program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

var
  I: Integer;
  SI: ShortInt; // -128..127
  B: Byte;      // 0..255
  W: Word;      // 0.65535
  C: Cardinal;   // 0..2147483648
  I164: Int64;
  LW: LongWord;

type
  TColor = (Rojo, Azul, Verde, Amarillo);
  TRangoColor = Azul..Amarillo;

var
  Color: TColor;
  esVerdadero: Boolean; //Booleano = (False, True) Los booleanos son enumerados, el 0 es false y 1 es true
  ColorRango: TRangoColor;

type
  TMenosDeCien = 0..99;
  Minuscula = 'a'..'z';
  Mayuscula = 'A'..'Z';

var
  R: Real;    //Deprecado
  S: Single;
  D: Double;  //Nativo, recomendado.
  E: Extended;
  Co: Comp;
  Cu: Currency;

var
  Debajo: TMenosDeCien;

begin
  //  ColorRango := Rojo; -> Excepcion
  //  Debajo := 100; -> Excepcion
  Color := Rojo;
  I := Ord(Azul);
  Writeln(I);
  esVerdadero := True;

  I := Trunc(1.2356);
  Writeln(I);
  I := Round(1.6);
  Writeln(I);

  Readln;
end.
