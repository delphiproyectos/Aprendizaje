program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

var
  cadena: string;
  num: Integer;
  num2: Integer;
  num3: Integer;
  I: Integer;
  numD: Double;
  numE: Extended;
  booleano: Boolean;


begin
  cadena := 'Hola mundo!';
  num := 45;
  num3 := 5;
  numD := 3.4;
  numE := 4.5;
  num2 := num + num3;
  booleano := (1 = 2);

  Writeln(num2);
  Writeln('');

  for I := 0 to 10 do
  begin
    Write('I := ');
    Writeln(I);
  end;
  Writeln('');

  cadena := 'Alberto';
  if cadena = 'Alberto' then
  begin
    Writeln('Hola Alberto');
  end;
  Writeln('');

  if cadena = 'Jose' then
  begin
    Writeln('Hola Jose');
  end
  else
  begin
    Writeln('Usuario desconocido');
  end;

  Readln;
end.
