program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TDia = 1..31;
  TDias = set of TDia;

var
  Dias: TDias;
  Semaforo: set of (Rojo, Amarillo, Verde);

begin
  Dias := [1,3,5];

  Dias := Dias + [7,8];

  Dias := Dias - [1,3];

  Dias := []; // Vaciar...

  Include(Dias, 11); // Igual que Dias := Dias + [11]
  Exclude(Dias, 5);  // Igual que Dias := Dias - [5]

  if (4 in Dias) then
  begin
    Writeln('"4" esta en el set');
  end else
  begin
    Writeln('"4" no esta en el set');
  end;

  Readln;

  Semaforo := [Rojo];

end.
