program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TPersona = class // Todas las clases vienen de TObject
    Nombre: string;
    Apellido: string;
    function NombreCompleto: string;
  end;

function TPersona.NombreCompleto: string;
begin
  Result := Nombre + ' ' + Apellido;
end;

var
  Persona: TPersona;

begin
  Persona := TPersona.Create;
  try
    Persona.Nombre := 'Alberto';
    Persona.Apellido := 'Caro';
    Writeln('Nombre completo: ' + Persona.NombreCompleto);
  finally
    Persona.Free; // Libera la memoria, importante!!
  end;
  Readln;
end.
