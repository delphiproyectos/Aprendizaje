unit Clases;

interface

type
  TClase = class(TObject)

  end;

  TDemo = class(TObject)
  strict private
    procedure Iniciar;
  public
    Clase: TClase;
    Numero: Integer;
    Cadena: string;
    constructor Create;
    constructor CreateValores(aNumero: Integer; aCadena: string);
    destructor Destroy; override;
    procedure Escribe;
  end;

implementation

{ TDemo }

constructor TDemo.Create;
begin
  inherited Create;
  Numero := 7;
  Cadena := 'FooBar';
  Iniciar;
end;

constructor TDemo.CreateValores(aNumero: Integer; aCadena: string);
begin
  inherited Create;
  Numero := aNumero;
  Cadena := aCadena;
  Iniciar;
end;

destructor TDemo.Destroy;
begin
  Clase.Free;
  inherited Destroy;
end;

procedure TDemo.Iniciar;
begin
  Clase := TClase.Create;
end;

procedure TDemo.Escribe;
begin
  Writeln('Numero: ', Numero);
  Writeln('Cadena: ', Cadena);
end;

end.
