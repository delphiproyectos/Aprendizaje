program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Clases in 'Clases.pas';

var
  DemoClase: TDemo;

begin
  DemoClase := TDemo.CreateValores(10, 'Hola mundo');
  try
    DemoClase.Escribe;
  finally
    DemoClase.Free;
  end;
  Readln;
end.
