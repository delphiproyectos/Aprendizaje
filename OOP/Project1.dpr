program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TPersona = class // Todas las clases vienen de TObject
    Nombre: string;
    Apellido: string;
    function NombreCompleto: string; virtual;
  end;

  TEmpleado = class(TPersona)
    ID: Integer;
    function NombreCompleto: string; override;
  end;

  TCliente = class(TPersona)
    ClienteID: Integer;
    function NombreCompleto: string; override;
  end;

  TClienteEspecial = class(TCliente)
    CodigoEspecial: Integer;
  end;
{ TPersona }

function TPersona.NombreCompleto: string;
  begin
    Result := Nombre + ' ' + Apellido;
  end;

{ TEmpleado }

function TEmpleado.NombreCompleto: string;
begin
  Result := 'Empleado: ' + inherited NombreCompleto;
end;


{ TCliente }

function TCliente.NombreCompleto: string;
begin
  Result := 'Cliente: ' + inherited NombreCompleto;
end;

function CrearSaludo(unaPersona: TPersona) : string;
begin
  Result := 'Hola ' + unaPersona.NombreCompleto;
end;

var
  Persona: TPersona;
  Empleado: TEmpleado;
  Cliente: TCliente;
  ClienteEspecial: TClienteEspecial;

begin
  Persona := TPersona.Create;
  Empleado := TEmpleado.Create;
  Cliente := TCliente.Create;
  ClienteEspecial := TClienteEspecial.Create;
  try
    Persona.Nombre := 'Alberto';
    Persona.Apellido := 'Caro';
    Writeln(CrearSaludo(Persona));

    Empleado.Nombre := 'Carlos';
    Empleado.Apellido := 'Rodriguez';
    Writeln(CrearSaludo(Empleado));

    Cliente.Nombre := 'Andrea';
    Cliente.Apellido := 'Martinez';
    Writeln(CrearSaludo(Cliente));

    ClienteEspecial.Nombre := 'Luis';
    ClienteEspecial.Apellido := 'Fernandez';
    Writeln(CrearSaludo(ClienteEspecial));

  finally
    Persona.Free;
    Empleado.Free;
    Cliente.Free;
    ClienteEspecial.Free;
  end;
  Readln;
end.
