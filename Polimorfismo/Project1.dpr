program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TPersona = class // Todas las clases vienen de TObject
    Nombre: string;
    Apellido: string;
    function NombreCompleto: string; virtual;
  end;

  TEmpleado = class(TPersona)
    ID: Integer;
    function NombreCompleto: string; override;
  end;

  TCliente = class(TPersona)
    ClienteID: Integer;
    function NombreCompleto: string; override;
  end;


{ TPersona }

function TPersona.NombreCompleto: string;
  begin
    Result := Nombre + ' ' + Apellido;
  end;

{ TEmpleado }

function TEmpleado.NombreCompleto: string;
begin
  Result := 'ID: ' + IntToStr(ID) + ' - ' + inherited NombreCompleto;
end;


{ TCliente }

function TCliente.NombreCompleto: string;
begin
  Result := 'Cliente: ' + inherited NombreCompleto;
end;

var
  Persona: TPersona;
  Empleado: TEmpleado;
  Persona2: TPersona;

begin
  Persona2 := TEmpleado.Create;
  try
    Persona2.Nombre := 'Manolo';
    Persona2.Apellido := 'Rodriguez';
    {Persona2.ID := 1;}          {Excepcion, esta declarado como tipo TPersona,
                                  por lo que no tiene ID }
    Writeln(Persona2.NombreCompleto); // Llamara a la funcion de TEmpleado
  finally
    Persona2.Free;
  end;
  Readln;
end.
