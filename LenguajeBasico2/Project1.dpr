program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

var
  I: Integer;
begin
  I := 0;
  if I = 0 then
  begin
    Writeln('I vale 0');
  end;

  I := 1;
  case I of
    1: Writeln('I vale 1');
    2: Writeln('I vale 2');
    3: begin

       end;
  end;
  I := 0;
  Writeln('');

  Writeln('Bucle repeat');
  repeat     //Parecido al Do while, se ejecuta al menos 1 vez
    I := I + 1;
    Writeln('I = ', I);
  until (I = 10);
  I := 0;
  Writeln('');

  Writeln('Bucle while');
  while I < 10 do
  begin
    I := I + 1;
    Writeln('I = ', I);
  end;
  Readln;
end.
