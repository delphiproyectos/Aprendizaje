program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

var
  Arr: array[0..9] of Integer;
  MultiArr: array[0..9, 0..9] of Char;
  ArrDeArr: array[0..9] of array[0..9] of Char; // Otra forma de escribir lo de arriba
  ArrI: array[5..7] of Integer; //No tiene por que empezar por 0
  I: Integer;
  ArrDinamico: array of string;
  Cantidad: Integer;

type
  TColor = (Rojo, Azul, Verde);

var
  ColorString: array[TColor] of string;
  ColorTipo: TColor;

begin
  Cantidad := 10;
  SetLength(ArrDinamico, Cantidad); // Ira de 0 a 10

  ArrDinamico[0] := 'Hola';
  ArrDinamico[1] := 'Mundo';

  I := Length(ArrDinamico);

  // Libera la memoria
  ArrDinamico := nil;
  Finalize(ArrDinamico);


  ArrI[5] := 14;
  ArrI[6] := 15;
  ArrI[7] := 44;

  ColorString[Rojo] := 'Rojo';
  ColorString[Azul] := 'Azul';
  ColorString[Verde] := 'Verde';

  for ColorTipo := Low(TColor) to High(TColor) do
  begin
    WriteLn(ColorString[ColorTipo]);
  end;

  MultiArr[0,0] := 'A';
  MultiArr[0][1] := 'B'; // Es igual que MultiArr[0,1]

  for I := Low(ArrI) to High(ArrI) do
  begin
    Writeln(ArrI[I]);
  end;
  Readln;
end.
