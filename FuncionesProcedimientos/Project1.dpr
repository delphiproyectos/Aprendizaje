program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

procedure EscribeLinea(Texto: string); // Equivalente a void
begin
  Writeln(Texto);
end;

function DevuelveNombre(Texto: string): string; // Debe devolver algo usando Result
begin
  Result := Texto + ' ' + 'Alberto Caro';
end;

begin
  EscribeLinea('Hola mundo');
  Writeln(DevuelveNombre('Mi nombre es'));
  Readln;
end.
