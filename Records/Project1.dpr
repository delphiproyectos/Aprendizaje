program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TPersona = record
    Apellido: string;
    Nombre: string;
    Edad: Integer;
  end;

var
  Persona: TPersona;

function CreaNombreCompleto(Persona: TPersona) : string;
begin
  Result := Persona.Nombre + ' ' + Persona.Apellido;
end;

begin
  Persona.Nombre := 'Alberto';
  Persona.Apellido := 'Caro';
  Persona.Edad := 21;
end.
