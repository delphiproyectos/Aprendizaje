program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TPersona = class // Todas las clases vienen de TObject
    Nombre: string;
    Apellido: string;
    function NombreCompleto: string; virtual;
  end;

  TEmpleado = class(TPersona)
  private
    ID: Integer;
    FSalarioMensual: Double;
    procedure AsignarSalario(const Value: Double);
    function ObtenerSalario: Double;
    function ObtenerSalarioMensual: Double;
  published
    function NombreCompleto: string; override;
    property SalarioAnual: Double read ObtenerSalario write AsignarSalario;
    property SalarioMensual: Double read ObtenerSalarioMensual;
  end;

{ TPersona }

function TPersona.NombreCompleto: string;
  begin
    Result := Nombre + ' ' + Apellido;
  end;

{ TEmpleado }

procedure TEmpleado.AsignarSalario(const Value: Double);
begin
  FSalarioMensual := Value / 12;
end;

function TEmpleado.ObtenerSalario: Double;
begin
 Result := FSalarioMensual * 12;
end;

function TEmpleado.ObtenerSalarioMensual: Double;
begin
  Result := FSalarioMensual;
end;

function TEmpleado.NombreCompleto: string;
begin
  Result := 'Empleado: ' + inherited NombreCompleto;
end;

var
  Empleado: TEmpleado;
  Temporal: Double;

begin
  Empleado := TEmpleado.Create;
  try
    Empleado.Nombre := 'Alberto';
    Empleado.Apellido := 'Caro';
    Empleado.SalarioAnual := 120000.00;
    Writeln(Empleado.NombreCompleto + ' - Salario anual: ' + FloatToStr(Empleado.SalarioAnual));
    Writeln('Salario mensual: ' + FloatToStr(Empleado.ObtenerSalarioMensual));
    Write('Escribe el nuevo salario: ');
    Readln(Temporal);
    Empleado.SalarioAnual := Temporal;
    Writeln(Empleado.NombreCompleto + ' - Nuevo Salario anual: ' + FloatToStr(Empleado.SalarioAnual));
    Writeln('Salario mensual: ' + FloatToStr(Empleado.ObtenerSalarioMensual));
  finally
    Empleado.Free;
  end;
  Readln;
end.
