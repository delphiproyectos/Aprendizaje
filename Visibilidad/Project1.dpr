program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  ClasesHijas in 'ClasesHijas.pas',
  ClasesBase in 'ClasesBase.pas';
var
  Prueba: TClaseBase;
begin
  Prueba := TClaseBase.Create;
  try
    Prueba.Foo;
    //Prueba.Cadena; -> Excepcion: Esta protegido.
    Prueba.NumeroD := 1.2; // Es publico
  finally
    Prueba.Free;
  end;
end.
