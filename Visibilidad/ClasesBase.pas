unit ClasesBase;

interface

type
  TClaseBase = class(TObject)
  private  {Solo disponibles en esta clase
            IMPORTANTE, si hay mas de una clase en un Unit, las clases podran ver las propiedades privadas de las demas
            Si se pone strict delante de private, se deshabilita esa caracteristica}
    Numero: Integer;
    FOtroNumero: Integer;
    procedure SetOtroNumero(const Value: Integer);
  protected // Disponibles en esta clase y sus descendientes, pero solo si no se usa
    Cadena: string;
  public // Disponibles en esta clase y sus descendientes
    NumeroD: Double;
    procedure Foo;
  published // Visible en RTTI, normalmente usado para propiedades.
    property OtroNumero: Integer read FOtroNumero write SetOtroNumero;
  end;

implementation

{ TClaseBase }

procedure TClaseBase.Foo;
begin
  Numero := 1;
end;

procedure TClaseBase.SetOtroNumero(const Value: Integer);
begin
  FOtroNumero := Value;
end;

end.
