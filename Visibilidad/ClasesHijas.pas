unit ClasesHijas;

interface
uses ClasesBase;
type
  TClaseHija = class(TClaseBase)
  public
    procedure Bar;

  end;

implementation

{ TClaseHija }

procedure TClaseHija.Bar;
begin
  // Numero := 1; -> Excepcion: Numero es privado
  Cadena := 'FooBar';
  NumeroD := 12.35;
end;

end.
