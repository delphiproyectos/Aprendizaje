program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

type
  TPersona = class // Todas las clases vienen de TObject
    Nombre: string;
    Apellido: string;
    function NombreCompleto: string; virtual;
  end;

  TEmpleado = class(TPersona)
    ID: Integer;
    function NombreCompleto: string; override;
  end;

function TPersona.NombreCompleto: string;
  begin
    Result := Nombre + ' ' + Apellido;
  end;

function TEmpleado.NombreCompleto: string;
begin
  Result := 'ID: ' + IntToStr(ID) + ' - ' + inherited NombreCompleto;
end;

var
  Persona: TPersona;
  Empleado: TEmpleado;

begin
  Persona := TPersona.Create;
  try
    Persona.Nombre := 'Alberto';
    Persona.Apellido := 'Caro';
    Writeln(Persona.NombreCompleto);
  finally
    Persona.Free; // Libera la memoria, importante!!
  end;
  Empleado := TEmpleado.Create;
  try
    Empleado.Nombre := 'Carlos';
    Empleado.Apellido := 'Hernandez';
    Empleado.ID := 1;
    Writeln(Empleado.NombreCompleto);
  finally
    Empleado.Free;
  end;
  Readln;
end.
