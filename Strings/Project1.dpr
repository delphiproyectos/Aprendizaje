program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

var
  S: string;
  S1, S2, S3: string;
  SS: ShortString;
  C: Char;
  Len: Integer;

begin
  S := 'Ejemplo de cadena'; // Siempre usar comillas simples, no dobles.
  S1 := ', otro ejemplo de cadena';

  SS := 'Esto es una cadena corta';
  S2 := S + S1;
  C := S[1]; // Primer caracter
  Len := Length(S);

  Writeln(S2);
  Writeln(SS);
  Writeln(C);
  Writeln(Len);

  Len := Ord(SS[0]);
  Writeln(Len);

  S3 := Copy(S, 1, 7);
  Writeln(S3);

  S := 'Escape de '' comilla';
  Writeln(S);
  Readln;
end.
